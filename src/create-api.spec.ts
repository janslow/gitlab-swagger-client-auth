import "should";
import "should-sinon";

import { spy, stub, SinonSpy } from "sinon";

import Config from "./model/config";
import createApi from "./create-api";

function createConfig(baseUrl: string, token: string): Config {
  return {
    gitlab: {
      baseUrl,
      token,
    }
  };
}

describe("create-api", () => {
  let fakeApiInstance: any;
  let fakeApiConstructor: SinonSpy;

  beforeEach(() => {
    fakeApiInstance = { _: "fakeApiInstance" };
    fakeApiConstructor = stub().returns(fakeApiInstance);
  });

  it("constructs an instance of an API with default fetch", () => {
    const baseUrl = "foo";
    const token = "token123";

    const actual = createApi(createConfig(baseUrl, token), <any> fakeApiConstructor);

    actual.should.be.exactly(fakeApiInstance);
    fakeApiConstructor.should.be.calledWithNew();
    fakeApiConstructor.should.be.calledOnce();
    fakeApiConstructor.should.be.calledWith({ privateToken: token }, undefined, baseUrl);
  });

  it("constructs an instance of an API with custom fetch", () => {
    const baseUrl = "bar";
    const token = "token321";

    const fetch = spy();
    const actual = createApi(createConfig(baseUrl, token), <any> fakeApiConstructor, fetch);

    actual.should.be.exactly(fakeApiInstance);
    fakeApiConstructor.should.be.calledWithNew();
    fakeApiConstructor.should.be.calledOnce();
    fakeApiConstructor.should.be.calledWith({ privateToken: token }, fetch, baseUrl);
    fetch.should.not.be.called();
  });
});
