import { FetchAPI, GitLabAuth, BaseAPI } from "gitlab-swagger-client";

import Config from "./model/config";

export interface ApiConstructor<T extends BaseAPI> {
  new(auth: GitLabAuth, fetch?: FetchAPI, basePath?: string): T;
}

export default function createApi<T extends BaseAPI>(config: Config, apiClass: ApiConstructor<T>, fetch?: FetchAPI): T {
  const { baseUrl, token } = config.gitlab;
  return new apiClass({ privateToken: token }, fetch, baseUrl);
};
