import { FetchAPI, BaseAPI } from "gitlab-swagger-client";

import Config from "./model/config";
import readRawConfig from "./services/rc";
import assertConfig from "./services/assert";
import rawCreateApi, { ApiConstructor } from "./create-api";

function readConfig(): Config {
  const { config } = readRawConfig<Config>({
    gitlab: {
      baseUrl: "https://gitlab.com/api/v3",
    },
  });
  assertConfig(config);
  return config;
}

let cachedCreateApi: <T extends BaseAPI>(apiClass: ApiConstructor<T>, fetch?: FetchAPI) => T = undefined;

export default function createApi<T extends BaseAPI>(apiClass: ApiConstructor<T>, fetch?: FetchAPI): T {
  if (cachedCreateApi === undefined) {
    const config = readConfig();
    cachedCreateApi = (apiClass: ApiConstructor<T>, fetch: FetchAPI) => rawCreateApi(config, apiClass, fetch);
  }
  return cachedCreateApi(apiClass, fetch);
}
