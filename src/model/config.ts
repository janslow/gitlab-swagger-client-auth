export interface GitLabConfig {
  baseUrl: string;
  token: string;
}

interface Config {
  gitlab: GitLabConfig;
}
export default Config;
