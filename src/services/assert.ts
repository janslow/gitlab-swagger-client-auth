import nodeAssert = require("assert");

import Config, { GitLabConfig } from "../model/config";
import debug from "./debug";

type PrimitiveType = "string"|"number"|"object"|"array";

const PROPERTY_NAME_SYMBOL = Symbol("propertyName");
const PROPERTY_VALUE_SYMBOL = Symbol("propertyValue");

function createProxy(base: any, baseName: string): any {
  return new Proxy({ base }, {
    get: (target: { base: any }, propertyName: string|symbol): any => {
      if (propertyName === PROPERTY_VALUE_SYMBOL) {
        return target.base;
      }
      else if (propertyName === PROPERTY_NAME_SYMBOL) {
        return baseName;
      }
      else if (typeof target.base !== "object") {
        throw new Error(`${baseName} is not an object.`);
      }
      else if (typeof propertyName === "symbol") {
        throw new Error(`Symbol properties (e.g., ${propertyName.toString()}) are not support.`);
      }
      else {
        return createProxy(target.base[propertyName], baseName === undefined ? propertyName : `${baseName}.${propertyName}`);
      }
    },
  });
}

export function getProperty<T, V>(options: T, property: (obj: T) => V): [string, V] {
  const proxy = createProxy(options, undefined);
  const prop: any = property(proxy);
  return [prop[PROPERTY_NAME_SYMBOL], prop[PROPERTY_VALUE_SYMBOL]];
}

function assert<T, V>(options: T, property: (obj: T) => V, predicate: (value: V) => boolean, toBe: string): void {
  const [name, value] = getProperty(options, property);
  debug(`option "${name}" = ${value}`);
  nodeAssert(predicate(value), `Expected "${name}" to be ${toBe}; was "${value}".`);
}

function assertType<T>(options: T, property: (obj: T) => any, type: PrimitiveType): void {
  assert(options, property, v => (typeof v) === type, `a ${type}`);
}

function assertNonEmptyString<T>(options: T, property: (obj: T) => string): void {
  assertType(options, property, "string");
  assert(options, property, x => x.length > 0, "a non-empty string");
}

export default function assertConfig(config: Config): void {
  assertNonEmptyString(config, x => x.gitlab.baseUrl);
  assertNonEmptyString(config, x => x.gitlab.token);
}
