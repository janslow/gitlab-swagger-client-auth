import rc = require("rc");

import debug from "./debug";

export const APP_NAME = "gitlab-swagger-client-auth";

interface RCMetadata {
  configs?: string[];
  config?: string;
}

export interface LoadedConfig<T> {
  config: T;
  paths: string[];
}

export default function read<T extends Object>(defaultOptions: Object): LoadedConfig<T> {
  const result: RCMetadata & T = <any> rc(APP_NAME, defaultOptions);
  debug(`Loaded config for ${APP_NAME}`, result);
  const { configs: paths } = result;
  delete result.config;
  delete result.configs;
  return {
    config: result,
    paths,
  };
}
