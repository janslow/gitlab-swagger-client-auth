import debug = require("debug");

const NAMESPACE = "gitlab-swagger-client-auth";

const debugApp = debug(NAMESPACE);
export default debugApp;
