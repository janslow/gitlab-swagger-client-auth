import "should";

import should = require("should");

import Config from "../model/config";
import assertConfig, { getProperty } from "./assert";

describe("assert", () => {
  describe("assertConfig", () => {
    function itThrowsErrorIf(description: string, config: Config): void {
      it(`throws an error if ${description}`, () => {
        (() => assertConfig(config)).should.throwError();
      });
    }
    it("returns for a valid config", () => {
      assertConfig({
        gitlab: {
          baseUrl: "http://base/url",
          token: "a-token",
        },
      });
    });
    itThrowsErrorIf("the config is undefined", undefined);
    itThrowsErrorIf("the config is empty", <any> {});
    itThrowsErrorIf("the config is missing the 'gitlab' config", <any> { foo: {} });
    itThrowsErrorIf("the config is missing the 'gitlab.baseUrl' config", {
      gitlab: <any> {
        token: "foo",
      },
    });
    itThrowsErrorIf("the config is missing the 'gitlab.token' config", {
      gitlab: <any> {
        baseUrl: "foo",
      },
    });
    itThrowsErrorIf("the 'gitlab.token' property is not a string", {
      gitlab: {
        baseUrl: "baseUrl",
        token: <any> 123,
      },
    });
    itThrowsErrorIf("the 'gitlab.baseUrl' property is not a string", {
      gitlab: {
        baseUrl: <any> 123,
        token: "token",
      },
    });
  });

  describe("getProperty", () => {
    it("returns the path and value for an existing property", () => {
      const [path, value] = getProperty({ foo: { bar: { foobar: 123 }}}, x => x.foo.bar.foobar);
      path.should.equal("foo.bar.foobar");
      value.should.equal(123);
    });
    it("returns the path and undefined for a non-existent property", () => {
      const [path, value] = getProperty({ foo: <any> undefined }, x => x.foo);
      path.should.equal("foo");
      should.not.exist(value);
    });
    it("throws an error for a non-existent nested property", () => {
      const f = () => getProperty({ foo: <any> undefined }, x => x.foo.bar);
      f.should.throwError("foo is not an object.");
    });
    it("throws an error for a symbol property", () => {
      const sym = Symbol("foo");
      const f = () => getProperty({ [sym]: <any> undefined }, (x: any) => x[sym]);
      f.should.throwError("Symbol properties (e.g., Symbol(foo)) are not support.");
    });
  });
});
