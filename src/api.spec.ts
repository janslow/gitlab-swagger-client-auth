import "should";

import { resolve } from "path";
import { writeFileSync, lstatSync, unlinkSync, Stats } from "fs";
import { GroupsApi } from "gitlab-swagger-client";

export type GitLabTokenSpec = (gitlabToken: () => string) => void;
export function describeWithGitLabToken(description: string, spec: GitLabTokenSpec): void {
  describe(description, function (): void {
    this.slow(10000);
    this.timeout(20000);

    let gitlabToken: string;
    before(function () {
      gitlabToken = process.env.npm_package_config_gitlab_token;
      if (!gitlabToken) {
        console.warn(`WARNING: GitLab token for testing not set, so some tests have been skipped.`);
        this.skip();
      }
    });

    (<GitLabTokenSpec> spec.bind(this))(() => gitlabToken);
  });
}

describeWithGitLabToken("API (integration)", function (gitlabToken: () => string) {
  it("lists projects in a group", async function (): Promise<void> {
    const groupsApi = new GroupsApi({ privateToken: gitlabToken() });
    const groupPath = "gitlab-org";

    const projects = await groupsApi.listGroupProjects({ id: groupPath });

    projects.should.be.an.Array();
    const gitlabCEProject = projects.find(x => x.path === "gitlab-ce");
    gitlabCEProject.should.be.an.Object();
  });
});
