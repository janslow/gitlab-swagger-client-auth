import "should";
import "should-sinon";

import { resolve } from "path";
import { renameSync, unlinkSync, writeFileSync, lstatSync } from "fs";
import { stub, spy } from "sinon";

import Config from "./model/config";
import readConfig, { APP_NAME } from "./services/rc";
import createApi from "./index";

describe("create API (functional)", () => {
  const renamedFiles: { originalPath: string, tmpPath: string }[] = [];
  before(() => {
    const tmpPathPrefix = `.tmp${Math.round(Math.random() * 10000)}`;
    const { paths = [] } = readConfig({});
    paths.forEach(originalPath => {
      const tmpPath = `${originalPath}${tmpPathPrefix}`;
      try {
        renameSync(originalPath, tmpPath);
        renamedFiles.push({ originalPath, tmpPath });
      }
      catch (err) {
        console.error(`Unable to move existing config file (${originalPath}) to tmp file (${tmpPath}).`, err);
        throw err;
      }
    });
  });

  after(() => {
    let err: any = undefined;
    renamedFiles.forEach(({ originalPath, tmpPath }) => {
      try {
        renameSync(tmpPath, originalPath);
      }
      catch (err) {
        console.error(`Unable to restore existing config file (${originalPath}) from tmp file (${tmpPath}).`, err);
      }
    });
    if (err) {
      throw err;
    }
  });

  let uncachedCreateApi: typeof createApi;
  beforeEach(() => {
    delete require.cache[require.resolve("./index")];
    uncachedCreateApi = require("./index").default;
  });

  function describeWithConfigFile(path: string): void {
    describe(`with config file "${path}"`, () => {
      afterEach(() => {
        try {
          if (!lstatSync(path).isFile()) {
            return;
          }
        } catch (err) {
          return;
        }
        unlinkSync(path);
      });

      function itCreatesAnAPIUsingAValid(configFileDescription: string, createConfig?: (token: string, baseUrl?: string) => void): void {
        it(`creates an API using a valid ${configFileDescription}`, createConfig && (() => {
          const baseUrl = `my-base-url - ${path}`;
          const token = `my-token - ${path}`;

          createConfig(token, baseUrl);

          const apiInstance = { _x: "apiConstructor" };
          const apiConstructor = stub().returns(apiInstance);
          const fetch = spy();
          uncachedCreateApi(<any> apiConstructor, fetch).should.be.exactly(apiInstance);

          fetch.should.not.be.called();
          apiConstructor.should.be.calledWithNew();
          apiConstructor.should.be.calledWith({ privateToken: token }, fetch, baseUrl);
        }));
        it(`creates an API using a valid ${configFileDescription} with default base URL`, createConfig && (() => {
          const token = `my-token - ${path}`;

          createConfig(token);

          const apiInstance = { _x: "apiConstructor" };
          const apiConstructor = stub().returns(apiInstance);
          const fetch = spy();
          uncachedCreateApi(<any> apiConstructor, fetch).should.be.exactly(apiInstance);

          fetch.should.not.be.called();
          apiConstructor.should.be.calledWithNew();
          apiConstructor.should.be.calledWith({ privateToken: token }, fetch, "https://gitlab.com/api/v3");
        }));
        it(`caches the config creator to avoid repeating file access`, createConfig && (() => {
          const baseUrl = `my-base-url - ${path}`;
          const token = `my-token - ${path}`;

          createConfig(token, baseUrl);

          const apiConstructor1 = spy();
          const fetch1 = spy();
          uncachedCreateApi(<any> apiConstructor1, fetch1);

          apiConstructor1.should.be.calledWithNew();
          apiConstructor1.reset();

          // Delete config file to ensure the cached config is used
          unlinkSync(path);

          const apiInstance2 = { _x: "apiInstance2" };
          const apiConstructor2 = stub().returns(apiInstance2);
          const fetch2 = spy();
          uncachedCreateApi(<any> apiConstructor2, fetch2).should.be.exactly(apiInstance2);

          fetch1.should.not.be.called();
          fetch2.should.not.be.called();
          apiConstructor1.should.not.be.called();
          apiConstructor2.should.be.calledWithNew();
          apiConstructor2.should.be.calledWith({ privateToken: token }, fetch2, baseUrl);
        }));
      }

      itCreatesAnAPIUsingAValid("JSON config file", (token, baseUrl) => {
        const config: Config = {
          gitlab: {
            baseUrl,
            token,
          }
        };
        if (baseUrl === undefined) {
          delete config.gitlab.baseUrl;
        }
        writeFileSync(path, JSON.stringify(config), { encoding: "utf8" });
      });
      itCreatesAnAPIUsingAValid("creates an API using a valid INI config file", (token, baseUrl) => {
        let ini = `[gitlab]\n  token = ${token}\n`;
        if (baseUrl !== undefined) {
          ini = `${ini}\n  baseUrl = ${baseUrl}\n`;
        }
        writeFileSync(path, ini, { encoding: "utf8" });
      });
      it("throws an error if there is an invalid config file", () => {
        writeFileSync(path, "Not a valid config", { encoding: "utf8" });

        const apiConstructor = spy();
        (() => uncachedCreateApi(<any> apiConstructor)).should.throwError();
        apiConstructor.should.not.be.called();
      });
    });
  }

  describeWithConfigFile(resolve(process.cwd(), ".gitlab-swagger-client-authrc"));
  describeWithConfigFile(resolve(process.cwd(), "../.gitlab-swagger-client-authrc"));
  describeWithConfigFile(resolve(process.env.HOME, ".gitlab-swagger-client-authrc"));

  describe("with no config file", () => {
    it("throws an error", () => {
      const apiConstructor = spy();
      (() => uncachedCreateApi(<any> apiConstructor)).should.throwError();
      apiConstructor.should.not.be.called();
    });
  });
});
