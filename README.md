# gitlab-swagger-client-auth

## Installation
```bash
npm install gitlab-swagger-client gitlab-swagger-client-auth -S
```

## Configuration
```json
// $HOME/.gitlab-swagger-client-authrc
{
  "gitlab": {
    "baseUrl": "https://gitlab.com/api/v3", // Defaults to https://gitlab.com/api/v3
    "token": "MY_API_TOKEN" // From https://gitlab.com/profile/account or https://gitlab.com/profile/personal_access_tokens
  }
}
```

The config file is loaded using [rc](https://www.npmjs.com/package/rc), so config is merged from the following JSON/INI files (if they exist, in order of precedence):

* Environment variables (e.g., `$gitlab_swagger_client_auth_gitlab__baseUrl`)
* A `.gitlab-swagger-client-authrc` in the current directory (or the first found by navigating up the CWD path)
* `$HOME/.gitlab-swagger-client-authrc`
* `$HOME/.gitlab-swagger-client-auth/config`
* `$HOME/.config/gitlab-swagger-client-auth`
* `$HOME/.config/gitlab-swagger-client-auth/config`
* `/etc/gitlab-swagger-client-authrc`
* `/etc/gitlab-swagger-client-auth/config`

## Usage
```javascript
import { GroupsApi } from 'gitlab-swagger-client';
import createGitlabApi from 'gitlab-swagger-client-auth';

const groupsApi = createGitlabApi(GroupsApi);
```

## Development
### Integration tests
To run the integration tests, you will need to provide an access token for the `gitlab.com` API ([obtained via the Web API](https://gitlab.com/profile/personal_access_tokens)). Run the following command to set the token in your NPM config:

```bash
npm config set gitlab-swagger-client-auth:gitlab-token TOKEN
```

Note, this is only used to make read requests against the `gitlab-org` group and doesn't access/modify any of your own data.
